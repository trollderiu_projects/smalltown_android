/* comment style */
/* compile on http://closure-compiler.appspot.com */

/*(function () {*/

function loadIntercepts() {

    var backpressed = false;
    SMLTOWN.inject.onBackPressed = function () {
        if ($("#smltown_console").hasClass("smltown_consoleExtended")) {
            $("#smltown_console").removeClass("smltown_consoleExtended");
        } else {
            if ($("#smltown_game").length) {
                SMLTOWN.Load.showPage("gameList");
            } else {
                if (backpressed) { /*let double tap on error*/
                    callDeviceFunction("closeGame");
                }
                SMLTOWN.Message.flash('_backAgain');
                backpressed = true;
                setTimeout(function () {
                    backpressed = false;
                }, 1600);
            }
        }
    };

    /*disable resize functions*/
    $(window).off("resize");
    $(window).resize(function (e) {
        e.preventDefault();
        /*disable function*/
    });
    SMLTOWN.Events.windowResize = function () {
    };
    SMLTOWN.Events.gameResize = function () {
    };

    /*GAME-LIST //////////////////////////////////////////////////////////////*/

    Intercept("SMLTOWN.Load.gameList", function () {
        /*fast game redirection*/
        callDeviceFunction("exitGame"); /*gameRedirection changes too*/
        clearTimeout(SMLTOWN.inject.vibrationInterval);

        if (!$("#changeServer").length) {
            var changeServer = $("<a id='changeServer'>↺ Change server</a>");
            $("#smltown_footer").append(changeServer);
            changeServer.click(function () {
                SMLTOWN.Message.setLog("loading server list...");
                callDeviceFunction("savePreference", "serverRedirection", "0");
                callDeviceFunction("loadUrl", "file:///android_asset/index.html");
            });
        }
    });

    Intercept("SMLTOWN.Games.access", function (id) {
        callDeviceFunction("gameRun", "" + id); /*clean notifications and save game redirection*/
        callDeviceFunction("clearNotification", id);
    });

    Intercept("SMLTOWN.Games.list", function (gameList) {
        SMLTOWN.inject.notifications = callDeviceFunction("getNotifications");
        /*clear all notifications on show game list*/
        callDeviceFunction("clearNotification", "");

        var list = [];
        try {
            list = JSON.parse(SMLTOWN.inject.notifications);
        } catch (e) {
            smltown_error("error on parse: " + list);
            return;
        }
        for (var i = 0; i < list.length; i++) {
            var note = list[i];
            var gameInfo = $("#" + note.id + " .smltown_gameInfo");
            var divCount = gameInfo.find(".smltown_count");

            /*1st notification*/
            if (!gameInfo.find(".smltown_notifications").length) {
                gameInfo.html("");

                /*title*/
                if (note.isChat) {
                    var small = $("<span>");
                    small.text(note.title + ": ");
                    small.css({
                        'color': "rgb(125,125,125)",
                        'font-size': "smaller"
                    });
                    gameInfo.append(small);
                }

                /*text*/
                var text = note.text
                if ("_" == text[0]) {
                    text = SMLTOWN.Message.translate(text.substr(1));
                }
                var span = $("<span class='smltown_notifications'>");
                span.text(text);
                gameInfo.append(span);
                continue;
            }

            if (!divCount.length) {
                /*count*/
                var span2 = $("<span>");
                span2.append(" (<span class='smltown_count'>2</span>)");
                gameInfo.append(span2);
                continue;
            }

            divCount.text(parseInt(divCount.text()) + 1);
        }

        callDeviceFunction("stopRefresh");
    }, true);

    Intercept("SMLTOWN.Games.swipeGameStart", function () {
        console.log("calling swipeGame true");
        callDeviceFunction("swipeGame", "true");
    });
    Intercept("SMLTOWN.Games.swipeGameEnd", function () {
        console.log("calling swipeGame empty");
        callDeviceFunction("swipeGame", "");
    });

    /*GAME ///////////////////////////////////////////////////////////////////*/
    SMLTOWN.inject.gameEvents = function () {
        var vibrationStrengthDiv = $("<div id='vibrationStrength' class='input'>"
                + "<span>VibrationStrength</span>"
                + "<form>"
                + "<input type='text' placeholder='" + SMLTOWN.user.vibrationStrength + "'/>"
                + "</form>"
                + "</div>");

        $("#smltown_updateName").after(vibrationStrengthDiv);

        $("#vibrationStrength form").submit(function () {
            var input = $(this).find("input");
            var val = input.val();
            if (!val) {
                val = 1;
            }
            callDeviceFunction("setVibrationStrength", val);
            SMLTOWN.Message.flash("_vibration strength saved");
            input.blur();
            return false;
        });

        SMLTOWN.Social.android.inPlay();
    };

    /*ON GAME START*/
    Intercept("SMLTOWN.Events.game", function () {
        SMLTOWN.inject.gameEvents();
    }, true); /*don't slow game and not parameter need*/

    Intercept("SMLTOWN.Update.getPicture", function (player) {
        if (player.type == "android" && player.socialId) {
            var picture = callDeviceFunction("getAndroidPicture", player.socialId);
            /*console.log("picture = " + picture);*/
            picture = "data:image/png;base64," + picture;
            player.div.find(".smltown_picture").append("<image src='" + picture + "'></img>");
            return false;
        }
    });

    Intercept("SMLTOWN.Server.request.setName", function (name) {
        callDeviceFunction("savePreference", "userName", name);
    });

    Intercept("SMLTOWN.Social.showFriends", function () {
        SMLTOWN.Social.android.showFriends();
    });

    /*VIBRATIONS*/
    window.SMLTOWN.inject.vibrate = function () {
        clearTimeout(SMLTOWN.inject.singleVibrationInterval);
        clearTimeout(SMLTOWN.inject.vibrationInterval);
        SMLTOWN.inject.vibrationInterval = setInterval(function () {
            callDeviceFunction("vibrate");
        }, 1500);
    };

    /*2 vibrations only*/
    window.SMLTOWN.inject.singleVibration = function () {
        clearTimeout(SMLTOWN.inject.singleVibrationInterval);
        callDeviceFunction("vibrate");
        /*SMLTOWN.inject.singleVibrationInterval = setTimeout(function () {
         callDeviceFunction("vibrate");
         }, 500);*/
    };

    Intercept("SMLTOWN.Message.notify", function (text, okCallback, cancelCallback, gameId) {
        if (!text) {
            return;
        }
        if (!gameId) {
            gameId = SMLTOWN.Game.info.id;
        }

        SMLTOWN.inject.singleVibration();

        var cleanText = text.replace(/<\/?[^>]+(>|$)/g, ""); /*remove html tags*/

        var title = 'game id ' + gameId;
        if (SMLTOWN.Game.info.id == gameId) {
            var gameName = SMLTOWN.Game.info.name;
            if ("undefined" == typeof gameName) {
                gameName = "Local";
            }
            title = '\"' + gameName + '\"';
        }

        /*prevent quote injection error*/
        cleanText = cleanText.replace("'", "\\'");
        callDeviceFunction("setNotification", cleanText, title, gameId);
    });

    Intercept("SMLTOWN.Action.startNightTurn", function () {
        /*clearTimeout(SMLTOWN.inject.singleVibrationInterval);*/
        /*prevent loop vibrations on reconnections*/
        if (!SMLTOWN.Server.reconnection) {
            SMLTOWN.inject.vibrate();
        }
    });

    Intercept("SMLTOWN.Message.addChat", function (text, playId, gameId, name) {
        if (!text || typeof playId == "undefined") {
            return;
        }
        var message = text.replace(/<\/?[^>]+(>|$)/g, ""); /*remove html tags*/
        if (!gameId) {
            gameId = "";
        }
        if (!name) {
            name = "";
        }

        var image = "";
        if (SMLTOWN.players[playId] && SMLTOWN.players[playId].card) {
            var card = SMLTOWN.players[playId].card.split("_").pop();
            image = SMLTOWN.inject
                    .getBase64Image(SMLTOWN.path + "games/" + SMLTOWN.Game.info.type + "/cards/" + card + ".jpg")
                    .replace("data:image/png;base64,", "");
            image = JSON.stringify(image);
        }
        message = message.replace(/\'/g, "\\'").replace(/\"/g, '\\"');
        callDeviceFunction("setChatNotification", message, name, gameId, image);
    });

    /*functions last intercept*/
    SMLTOWN.inject.canvas = document.createElement("canvas");
    SMLTOWN.inject.img1 = document.createElement("img");
    SMLTOWN.inject.getBase64Image = function (p) {
        SMLTOWN.inject.img1.setAttribute('src', p);
        var size = 128;
        SMLTOWN.inject.canvas.width = size;
        SMLTOWN.inject.canvas.height = size;
        var ctx = SMLTOWN.inject.canvas.getContext("2d");
        ctx.drawImage(SMLTOWN.inject.img1, 0, 0, size, size);
        return SMLTOWN.inject.canvas.toDataURL("image/jpg");
    };

    Intercept("SMLTOWN.Message.removeNotification", function () {
        clearTimeout(SMLTOWN.inject.vibrationInterval);
        if ($("#smltown_notification").length) {
            callDeviceFunction("clearNotification", SMLTOWN.Game.info.id);
        }
    });

    Intercept("SMLTOWN.Load.cleanGameErrors", function () {
        callDeviceFunction("clearCache");
    });

    /*tutorial*/
    Intercept("SMLTOWN.Help.tour", function () {
        localStorage.setItem("tutorial", "started");
    });

    Intercept("SMLTOWN.Help.done", function () {
        localStorage.setItem("tutorial", "done");
        /*SMLTOWN.Social.android.checkPhoneNumber();*/
    });

    Intercept("SMLTOWN.Social.linkCopyEvent", function () {
        $("#smltown_friendsLink").off(".linkCopyEvent");
        $("#smltown_friendsLink").tap(function (e) {
            var res = callDeviceFunction("copyToClipboard", location.origin + "/" + location.hash.split("?")[1]);
            console.log(res);
            SMLTOWN.Message.flash("_clipboardCopy");
        });
    });
}

/*INJECT*/
function ready() {
    console.log("injection ready()");
    /*LOADS*/
    SMLTOWN.inject = {};
    loadIntercepts();

    SMLTOWN.Social.android.start();

    var gameId = callDeviceFunction("getPreference", "gameRedirection");
    /*smltown_error("gameId redirection = " + gameId);*/
    if (parseInt(gameId)) {
        window.location.hash = "game?" + gameId;
    } else {
        window.location.hash = "gameList";
    }
    console.log("injection load url: " + window.location.hash);
}

/*Prevent bucles duplication*/
console.log("SMLTOWN: " + ("undefined" != typeof window.SMLTOWN) + ", SMLTOWN.inject: " + ("undefined" != typeof window.SMLTOWN));
if (window.SMLTOWN && !window.SMLTOWN.inject) {
    if ($.isReady) {
        ready();
    } else {
        $(document).one("ready", function () {
            ready();
        });
    }
} else {
    /*DEBUG*/
    /*smltown_error("caution: duplicate injection");*/
}

/*}());*/