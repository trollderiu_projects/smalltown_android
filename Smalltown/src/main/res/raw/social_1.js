/* comment style */
/* compile on http://closure-compiler.appspot.com */

SMLTOWN.Social.android = {
    start: function() {
        /*this.checkPhoneNumber();*/

        /*if not social id*/
        if (!SMLTOWN.user.socialId) {
            SMLTOWN.user.socialId = callDeviceFunction("getPreference", "phoneId"); /*gameRedirection changes too*/
        }

        if (SMLTOWN.user.socialId) {
            SMLTOWN.Server.addUser("android", SMLTOWN.user.socialId);
        } else {
            SMLTOWN.Server.addUser();
        }
    }
    ,
    inPlay: function() {
        $("#smltown_addSocialId").show();
        $("#smltown_updateImage").show();
        $("#smltown_updateImage").on("tap", function() {
            /*setProfileImage response*/
            smltown_error("social.js event");
            callDeviceFunction("pickUserImage");
        });
        $("#smltown_updateImage").on("click", function() {
            smltown_error("click event");
        });
    }
    /*,
    checkPhoneNumber: function() {
        if ("ask" == SMLTOWN.user.phoneId && "todo" != localStorage.getItem("tutorial")) {
            this.askPhoneNumber();
            SMLTOWN.Server.addUser();
            return;
        }
    }*/
    ,
    askPhoneNumber: function() {
        var $this = this;
        var lang = this.lang[SMLTOWN.lang];

        var note;
        if (!SMLTOWN.user.socialId) {
            note = lang.phoneHelp;
        } else {
            note = lang.phoneEdit;
        }

        SMLTOWN.Message.bottomDialog("set your phone to find friends", function(phone) { /*ok callback*/
            if (!phone) {
                $this.socialId = "";
                callDeviceFunction("savePreference", "phoneId", ""); /*store in prefs*/
                callDeviceFunction("savePreference", "phoneId", ""); /*store in prefs*/
            }

            var hash = callDeviceFunction("setPhone", phone);
            SMLTOWN.Server.addUser("android", hash);
            localStorage.setItem("phone", ""); /*not ask anymore*/

            if (phone && !SMLTOWN.Social.friends) {
                SMLTOWN.Social.android.findFriends();
            }
        }, note);

        $("#smltown_bottomDialog .smltown_dialogSubmit").val("not now");

        /*dialog will be removed, not off keyup?*/
        $("#smltown_bottomDialog").keyup(function() {
            var val = $(this).find(".smltown_dialogInput").val().replace(/ /g, '');
            if (val) {
                if (SMLTOWN.Util.isNumeric(val)) {
                    if (val.length < 7) {
                        $(this).find(".smltown_dialogSubmit").removeClass("smltown_dialogSend").val("too short");
                    } else {
                        $(this).find(".smltown_dialogSubmit").addClass("smltown_dialogSend").val("Ok");
                    }
                } else {
                    $(this).find(".smltown_dialogSubmit").removeClass("smltown_dialogSend").val("wrong");
                }
            } else {
                $(this).find(".smltown_dialogSubmit").removeClass("smltown_dialogSend").val("not now");
            }
        });
    }
    ,
    findFriends: function() {
        return false;
        /*void -> friendsLoaded*/
        /*if (SMLTOWN.user.socialId) {            
           var phonesId = callDeviceFunction("getFriendsPhonesId");

           if (phonesId) {
               SMLTOWN.Server.ajax({action: 'findFriends', friends: phonesId}, function(data) {
                    SMLTOWN.Social.android.friendsLoaded(data);
                }, 'php/social.php');
            }
        }*/
    }
    ,
    friendsLoaded: function(friends) {
        /*approximation of equal*/
        if (SMLTOWN.Social.friends && friends && SMLTOWN.Social.friends.length == friends.length) {
            return;
        }

        SMLTOWN.Server.loaded();
        callDeviceFunction("saveFriends", JSON.stringify(friends));

        SMLTOWN.Social.friends = friends;
        if (friends && SMLTOWN.Social.friends.length) {
            SMLTOWN.Social.showFriends();
        }
    }
    ,
    showFriends: function() {
        var $this = this;
        var lang = this.lang[SMLTOWN.lang];

        var players = [];
        for (var i = 0; i < SMLTOWN.players.length; i++) {
            var player = SMLTOWN.players[i];
            if ("android" == player.type) {
                players.push(player.socialId);
            }
        }

        /*console.log("friends = " + localStorage.getItem("smltown_userFriends"));
         console.log("friends = " + friends);*/
        $("#smltown_friendsContent").html("");

        /*let share at any case*/
        if (!$("#smltown_shareMenu .share").length) {
            var share = $("<div class='share'>");
            $("#smltown_shareMenu").append(share);
            share.append("<img src='img/icon_share.png'>");
            share.append("<span>Send game link</span>");
            share.click(function() {
                callDeviceFunction("share", location.href.replace("http://", ""));
            });
        }

        var socialFriends = SMLTOWN.Social.friends;

        if (null == socialFriends || !socialFriends.length) {
            /*this takes so long js work (set last)*/
            SMLTOWN.Social.android.findFriends();
            SMLTOWN.Message.flash(lang.noMatches);
            return;
        }

        var invitableFriends = 0;
        for (var i = 0; i < socialFriends.length; i++) {
            /*if playing*/
            var friend = socialFriends[i];
            if ($.inArray(friend.socialId, players)) {
                continue;
            }

            $this.android.invitableFriend(socialFriends[i]);
            invitableFriends++;
        }

        if (!invitableFriends) {
            SMLTOWN.Social.android.findFriends();
            SMLTOWN.Message.flash(lang.noMoreFriends);
            return;
        }

        $("#smltown_friendSelector .smltown_submit").click(function() {

            /*Get the list of selected friends*/
            var friends = [];
            var divFriends = $(".smltown_invitableFriend.active");
            for (var i = 0; i < divFriends.length; i++) {
                friends.push(divFriends.attr("socialId"));
            }

            var json = JSON.stringify(friends);
            SMLTOWN.Server.request.setPlayerNotifications(json, lang.comePlay);
        });

        $("#smltown_friendSelector .smltown_update").click(function() {
            SMLTOWN.Server.loading();
            SMLTOWN.Social.android.findFriends();
        });

        $("#smltown_friendSelector").show();
    }
    ,
    /*from device function*/
    setProfileImage: function(image) {
        SMLTOWN.Social.setPicture(image);
    }
    ,
    lang: {
        en: {
            'phoneHelp': "Your phone number won't be saved anywhere,<br/>"
                    + "only will share an unidirectional hash of it.",
            'phoneEdit': "You will override other entered phone number. <br/>"
                    + "That means your actual friends will not see you. <br/>"
                    + "(this application doesn't check number veracity)<br/>",
            'noMatches': "no matches found",
            'noMoreFriends': "no more friends to invite",
            'comePlay': "come to play my game!"
        }
        ,
        es: {
            'phoneHelp': "Your phone number won't be saved anywhere,<br/>"
                    + "only will share an unidirectional hash of it.",
            'phoneEdit': "You will override other entered phone number. <br/>"
                    + "That means your actual friends will not see you. <br/>"
                    + "(this application doesn't check number veracity)<br/>",
            'noMatches': "no se han encontrado coincidencias",
            'noMoreFriends': "no hay más amigos para invitar",
            'comePlay': "ven a jugar una partida!"
        }
    }
};

SMLTOWN.Social.addSocialId = function() {
    this.android.askPhoneNumber();
};
