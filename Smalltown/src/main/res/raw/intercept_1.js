/* comment style */
/* compile on http://closure-compiler.appspot.com */

/* injectionLoaded in first file load */
var injectionLoaded = true;

/*(function () {*/

    var Intercepted = {};

    window.Intercept = function (path, callback, before) {

        var array = path.split(".");
        var funcName = array.join("_");

        var func = window[array[0]];
        for (var i = 1; i < array.length; i++) {
            func = func[array[i]];
            if ("undefined" === typeof func) {
                smltown_error("error parsing function on intercept with name: " + path);
                return false;
            }
        }

        if ("function" !== typeof func) {
            smltown_error("can't intercept " + path);
            return;
        }

        if (Intercepted[funcName]) {
            smltown_error("duplicate js injection: " + funcName);
            return;
        }
        Intercepted[funcName] = func;
        interceptedFunction(path, callback, before);
    };

    /*1 function 4 every intercept*/
    function interceptedFunction(path, callback, before) {

        var array = path.split(".");
        var funcName = array.join("_");

        var f = function () { /*args*/
            if (before) { /*first original function*/
                return callback(Intercepted[funcName].apply(this, arguments));
            } else { /*first new functions*/
                var res = callback.apply(this, arguments);
                if (false != res) {
                    return Intercepted[funcName].apply(this, arguments);
                }
            }
        };

        var object = "window";
        for (var i = 0; i < array.length; i++) {
            object += "['" + array[i] + "']";
        }
        try {
            eval(object + " = f");
        } catch (e) {
            if (e instanceof SyntaxError) {
                smltown_error(e.message);
            }
        }

    };

    /*all console.log readable*/
    window.console.log = function (text) {
        if("object" == typeof text){
            text = JSON.stringify(text);
        }
        console.info.call(console, text);
    };

/*}());*/
