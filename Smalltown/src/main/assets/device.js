/*respect comment style*/

/* apply(arguments) FAILS */
function callDeviceFunction() {
    var response;
    var string = "(function(){ response = Device." + arguments[0] + "(";

    for (var i = 1; i < arguments.length; i++) {

        string += "'" + arguments[i] + "'";
        if (arguments.length > i + 1) {
            string += ",";
        }
    }
    string += ") })";

    try {
        eval(string)();
    } catch (e) {
        /*well tested error catch*/
        smltown_error(e.message + " ON: " + string);
    }
    return response;
}
