package gardiazabal.smltown;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.app.Activity;
import android.content.SharedPreferences;

public class BootReceiver extends BroadcastReceiver {

//    //only start activity receiber
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences prefs = context.getSharedPreferences("smalltown", Context.MODE_PRIVATE);
        if (!prefs.getString("gameRedirection", "").isEmpty()) {

//            Intent i = new Intent(context.getApplicationContext(), gardiazabal.smltown.MainActivity.class);
//        Bundle bundle = new Bundle();
//        i.putExtras(bundle);
//            i.addFlags(
//                    Intent.FLAG_ACTIVITY_NEW_TASK
//                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            context.getApplicationContext().startActivity(i);

            Intent i = new Intent(context, BootService.class);
            context.startService(i);

        }
    }
}
