package gardiazabal.smltown;

import android.util.Log;
import android.widget.*;
import android.view.inputmethod.*;
import android.view.*;
import android.content.*;
import android.webkit.*;
import android.view.ViewTreeObserver.*;
import gardiazabal.smltown.*;
import android.text.*;
import junit.framework.Assert;
import android.util.AttributeSet;

public class SmalltownLayout extends WebView {

    String logName;

    public SmalltownLayout(Context context) {
        super(context);
        logName = this.getClass().getName();
    }

    public SmalltownLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmalltownLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    MainActivity a;
    WebView webView = this;

    public void setMainActivity(MainActivity a) {
        this.a = a;
    }

    @Override
    public boolean dispatchKeyEventPreIme(KeyEvent event) {
        if (a != null && event.getKeyCode() == KeyEvent.KEYCODE_FORWARD_DEL) {
            int height = webView.getRootView().getHeight() - webView.getHeight();
            if (height > 100) {
                a.chatFocusOut();
            }
        }
        return super.dispatchKeyEventPreIme(event);
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        InputConnection inputConnection = super.onCreateInputConnection(outAttrs);
        if (outAttrs != null) {
            // remove other IME_ACTION_*
            outAttrs.imeOptions &= ~EditorInfo.IME_ACTION_GO;
            outAttrs.imeOptions &= ~EditorInfo.IME_ACTION_SEARCH;
            outAttrs.imeOptions &= ~EditorInfo.IME_ACTION_DONE;
            outAttrs.imeOptions &= ~EditorInfo.IME_ACTION_NONE;
            outAttrs.imeOptions &= ~EditorInfo.IME_ACTION_NEXT;
            // add IME_ACTION_NEXT instead
            outAttrs.imeOptions |= EditorInfo.IME_ACTION_SEND;
        }
        return inputConnection;
    }

    @Override
    public void loadUrl(String url) {
        Log.i(logName, "loadUrl string: " + url);
        super.loadUrl(url);
    }

}
