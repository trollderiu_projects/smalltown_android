package gardiazabal.smltown;

import android.app.Service;
import android.os.Binder;
import android.os.IBinder;
import android.content.Intent;
import android.app.NotificationManager;

//must be in main package to well reference ?
public class KillNotificationsService extends Service {

    public class KillBinder extends Binder {

        public final Service service;

        public KillBinder(Service service) {
            this.service = service;
        }

    }

    private NotificationManager mNM;
    private final IBinder mBinder = new KillBinder(this);

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        String ns = NOTIFICATION_SERVICE;
        if (null != ns) {
            mNM = (NotificationManager) getSystemService(ns);
            mNM.cancel(0);
        }
    }
}
