package gardiazabal.smltown;

import android.annotation.TargetApi;
import android.content.res.AssetManager;
import android.os.Vibrator;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.os.PowerManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings;
import android.webkit.CookieSyncManager;
import android.webkit.CookieManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.media.AudioManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.IOException;
//import gardiazabal.smltown.views.SmalltownLayout;
//
import android.view.View;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.*;

import gardiazabal.smltown.utils.*;
import android.util.*;
import java.io.InputStream;
import android.app.Activity;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.webkit.ConsoleMessage;
import android.support.v4.widget.*;
import android.view.*;

//public class MainActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener {
public class MainActivity extends Activity implements CustomSwipeRefreshLayout.OnRefreshListener {

    //android
    public static Context ctx;
    public static MainActivity mainActivity;
    private static String logName;

    SharedPreferences prefs;
    WebAppInterface webAppInterface;
    static PowerManager.WakeLock wakeLock;

    GameNotification gameNotification;
    SDCardCache sdCardCache;

    //custom
    static boolean gameInBackground = false;
    private boolean gameList = false;
    int vibrationLength = 1;
    long[] pat = {0, vibrationLength, 150, vibrationLength};
    Vibrator vibrator;
    static SmalltownLayout webView;
    String preInjection = "";

    boolean javascriptInterfaceBroken = false;
    private Handler mHandler;
    static Map<String, Boolean> BackPressed;
    String lastGameUrl = null;
    String referrerUrl = "";
    static String actualUrl = null;
    static String lastInjection = "";
    DeviceInfo deviceInfo;
    LocalServer localServer;

    public static SwipeRefreshLayout swipeLayout;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mainActivity = this;
        logName = this.getClass().getName();

        setContentView(R.layout.main);

        //WebView
        webView = (SmalltownLayout) findViewById(R.id.webview);

        webView.loadData("loading game... ", "text/html", "UTF-8");
        webView.setMainActivity(this);
        webView.setWebViewClient(new WebViewClient());

        if (Build.VERSION.SDK_INT >= 11) { //"View.LAYER_TYPE_HARDWARE" version
            if (Build.VERSION.SDK_INT >= 19) {
                webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }
        }

        //right white margin 2.3
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        super.onCreate(savedInstanceState); // after preferences creaed
        ctx = getApplicationContext();
        deviceInfo = new DeviceInfo(ctx);
        localServer = new LocalServer(ctx);

        //CPU ON (before notification!)
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SmltownWakelockTag");
        wakeLock.acquire();

        //start services
        mHandler = new Handler();
        BackPressed = new HashMap<String, Boolean>();
        BackPressed.put("recent", false);
        BackPressed.put("chat", false);

        prefs = ctx.getSharedPreferences("smalltown", Context.MODE_PRIVATE);
        webAppInterface = new WebAppInterface(this);

        String strength = webAppInterface.getPreference("vibrationStrength");
        Integer num = parseInt(strength);
        if (num != null) {
            vibrationLength = num;
        }
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        //invitation redirection   
        String url = getIntent().getDataString();
        Log.i(logName, "on create data = " + url);
        if (null != url) {
            // last '/' on simple case
            String[] simpleURL = url.split("/");
            String path = simpleURL[simpleURL.length - 1];
            //or last '?' on long case
            String[] longURL = path.split("\\?");
            String numberGame = longURL[longURL.length - 1];

            if (isNumeric(numberGame)) {
                prefs.edit().putString("gameRedirection", numberGame).commit();
                Log.i(logName, "game redirection: " + numberGame);
            }
        }

        //PREFS
        sdCardCache = new SDCardCache(ctx); //if clearApplication needed
        //check first installation time     
        if (!prefs.getBoolean("installed", false)) {
            String urlServer = "http://smltown.tk";

            String user[] = deviceInfo.getUserProfile(getContentResolver());
            String userId = user[0];
            String userName = user[1];
            String email = user[2];

            SharedPreferences.Editor edit = prefs.edit();
            edit.putString("userId", userId);
            edit.putString("userName", userName);
            edit.putString("userEmail", email);
            edit.putString("urls", "[\"" + urlServer + "\"]");
            edit.putString("serverRedirection", urlServer);
            edit.putBoolean("installed", true)
                    .commit(); //only needs commit lastone

            preInjection += "if(!localStorage.getItem('tutorial')){localStorage.setItem('tutorial', 'todo');};";
            preInjection += "if(!localStorage.getItem('phone')){localStorage.setItem('phone', 'ask');};";

        } //check version change
        else {
            PackageInfo pInfo = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (NameNotFoundException e) {
            }
            String version = pInfo.versionName;
            if (!version.equals(prefs.getString("version", null))) {
                sdCardCache.clearApplicationCache();
            }
        }

        //WebView
        try {
            if ("2.3".equals(Build.VERSION.RELEASE)) {
                javascriptInterfaceBroken = true;
            }
        } catch (Exception e) {
            // Ignore, and assume user javascript interface is working correctly.
        }

        // Add javascript interface only if it's not broken
        webView.setWebViewClient(new WebViewClient() {

            //@Override
            //public boolean onConsoleMessage(ConsoleMessage cm) {
            //    Log.i(logName, String.format("%s @ %d: %s", cm.message(), cm.lineNumber(), cm.sourceId()));
            //    return true;
            //}
            @Override
            public void onPageStarted(WebView view, String url, Bitmap icon) {
                super.onPageStarted(view, url, icon);

                if (!url.contains("android_asset")) {
                    //use cookies for inject BEFORE
                    //send cookie user data. global path = '/' 
                    String userId = prefs.getString("userId", "");

                    CookieSyncManager.createInstance(view.getContext());
                    CookieManager cookieManager = CookieManager.getInstance();
                    cookieManager.setAcceptCookie(true);
                    cookieManager.setCookie(url, "smltown_userId=" + userId);

                    CookieSyncManager.getInstance().sync();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i(logName, "url: " + url);

//                referrerUrl = "" + actualUrl; //(clone)
                actualUrl = url;
                gameList = false;

                //needs finished to get hash
                if (isInGame()) {
                    lastGameUrl = url;
                }
                //if facebook login return
                if (url.contains("return")) {
                    webView.loadUrl(lastGameUrl);
                    return;
                }

                // If running on 2.3, send javascript to the WebView to handle the function(s)
                if (javascriptInterfaceBroken) {
                    String handleGingerbreadStupidity = webAppInterface.handleGingerbreadStupidity;
                    view.loadUrl(handleGingerbreadStupidity);
                }
                // IF GAME OPEN
                if (!url.contains("android_asset")) {
                    String injection = ""
                            //                            + "document.addEventListener('DOMContentLoaded', function() {"
                            + "if(!window.injectionLoaded){";

                    //injection has to be with \" QUOTE
                    injection += preInjection;

                    //check if user exists
                    injection += "if(!window.SMLTOWN.user){window.SMLTOWN.user={};};";

                    //email
                    String userEmail = prefs.getString("userEmail", "");
                    if (!"".equals(userEmail)) {
                        injection += "SMLTOWN.user.mail = \"" + userEmail + "\";";
                    }

                    //name
                    String userName = prefs.getString("userName", "");
                    if (!"".equals(userName)) {
                        injection += "SMLTOWN.user.name = \"" + userName + "\";";
                    }

                    //friends
                    String friends = prefs.getString("friends", "");
                    if (!"".equals(friends)) {
                        injection += "SMLTOWN.Social.friends = \"" + friends + "\";";
                    }

                    //vibration
                    injection += "SMLTOWN.user.vibrationStrength = \"" + prefs.getString("vibrationStrength", "1") + "\";";  //injected

                    injection += "Device.getInjection();"
                            + "}" //end !injection loaded
                            //+ "});" //end dom loaded
                            + "";
                    updateGame(injection);

                    if (url.contains("#gameList")) {
                        gameList = true;
                    }
                }

                //stop loading refresh circle
                swipeLayout.setRefreshing(false);

                if (gameList) {

                    swipeLayout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener
                            = new ViewTreeObserver.OnScrollChangedListener() {
                                @Override
                                public void onScrollChanged() {
                                    if (webView.getScrollY() == 0) {
                                        swipeLayout.setEnabled(true);
                                    } else {
                                        swipeLayout.setEnabled(false);
                                    }

                                }
                            });
                    //default true
                    swipeLayout.setEnabled(true);

                } else {
                    swipeLayout.setEnabled(false);
                }
            }

            private static final String INJECTION_TOKEN = "__injection__";

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {                
                WebResourceResponse response = super.shouldInterceptRequest(view, url);
                Log.i(logName, "WebResourceResponse: " + url);
                if (url != null && url.contains(INJECTION_TOKEN)) {
                    String assetPath = url.substring(url.indexOf(INJECTION_TOKEN) + INJECTION_TOKEN.length(), url.length());
                    Log.i(logName, assetPath);
                    try {
                        response = new WebResourceResponse(
                                "application/javascript",
                                "UTF8",
                                ctx.getAssets().open(assetPath)
                        );
                    } catch (IOException e) {
                        e.printStackTrace(); // Failed to load asset file
                    }
                }
                return response;
            }

        });

        //ckeckIfPremium();
        //
        if (!javascriptInterfaceBroken) {
            webView.addJavascriptInterface(new WebAppInterface(this), "Device");
        }

        gameNotification = new GameNotification(ctx, enterReceiver, deleteReceiver, javascriptInterfaceBroken);
        //
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true); //HTML5 local storage?

        //let load loacl assets
//        webSettings.setAllowFileAccessFromFileURLs(true);
//        webSettings.setAllowUniversalAccessFromFileURLs(true);

//        Connectivity con = new Connectivity();
//        if (con.isConnected(ctx)) {
        webView.loadUrl("file:///android_asset/index.html");
//        } else {
//            webView.loadUrl("file:///android_asset/gameList.html");
//            //webView.loadUrl("<script>$('#log').html('')</script>");
//        }
//        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorScheme(android.R.color.holo_green_light);
        swipeLayout.setEnabled(false);
    }

    public void enableSwipeLayout() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setEnabled(true);
            }
        });
    }

    public void disableSwipeLayout() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setEnabled(false);
            }
        });
    }

    @Override
    public void onNewIntent(Intent intent) {
        Log.i(logName, "ON NEW INTENT");
        super.onNewIntent(intent);
        //prevents bug?
        //setIntent(intent);

        //if not data, can be default not in app intent 
        String url = intent.getDataString();
        if (null != url) {
            if (url.contains("?")) {
                String[] array = url.split("#");
                if (array.length > 1) {
                    String hash = array[1];
                    updateGame("location.hash = '" + hash + "'");
                }
            }
        }
    }

    public void vibration() {
        vibrator.vibrate(pat, -1);
    }

    // ACTION OVERRIDES //////////////////////////////////////////////////////
    //
    @Override
    public void onRefresh() {
        updateGame("SMLTOWN.Games.reloadList()");
    }

    //always on main thread
    public void stopRefreshLayout() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.i(logName, "actual url = " + actualUrl);

        if (isInGame()) {
            updateGame("if(SMLTOWN.inject){SMLTOWN.inject.onBackPressed()}"); //injected code, detect was injected

        } else if (null != actualUrl && actualUrl.contains("smltown.tk/forum/")) {
            webView.goBack();

        } else if (null != lastGameUrl) {
            webView.loadUrl(lastGameUrl);

        } else {
            webView.loadUrl("file:///android_asset/index.html");
        }
    }

    public boolean isInGame() {
        if (null == actualUrl) {
            return false;
        }
        return actualUrl.contains("/#game");
    }

    @Override
    public void onResume() {
        super.onResume();

        //mute
        AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mgr.setStreamMute(AudioManager.STREAM_SYSTEM, true);

        //re-load game
        gameInBackground = false;
        wakeLock.acquire();

        resumeGame();

        String game = prefs.getString("gameRedirection", "");
        if (!"".equals(game)) {
            addNotification(null, null, null, "", false);
        }
    }

    public void resumeGame() {
        //updateGame("SMLTOWN.Update.gameStatus()");
        mHandler.removeCallbacks(pingUpdate);//update ping
    }

    @Override
    public void onPause() {
        //un-mute
        AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mgr.setStreamMute(AudioManager.STREAM_SYSTEM, false);

        updateGame("SMLTOWN.Time.clearCountdowns()"); //stop countdown
        updateGame("SMLTOWN.Server.ping = " + 2000); //every 2sec.

        updateGameTimeout("SMLTOWN.Server.ping = 10000", 60000); //every 10sec / in 1min.
        updateGameTimeout("SMLTOWN.Server.ping = 60000", 300000); //every 1min / in 5min.
        super.onPause();
    }

    @Override
    public void onDestroy() {
        closeApplication();
        super.onDestroy();
    }

    @Override
    public File getCacheDir() {
        File cache = sdCardCache.getCacheDir();
        if (null != cache) {
            return cache;
        }
        return super.getCacheDir();
    }

    public static Uri imageUri;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 987 && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error?
                Log.i(logName, "image data = null");
                return;
            }
            Bitmap bitmap;

            try {
                if (null == data.getData()) {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                } else {
                    InputStream inputStream = ctx.getContentResolver().openInputStream(data.getData());
                    bitmap = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            // Convert bitmap to Base64 encoded image for web
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            //resize
            final int maxSize = 132;
//            int outWidth;
//            int outHeight;
//            int cropW = 0, cropH = 0;
//            int inWidth = bitmap.getWidth();
//            int inHeight = bitmap.getHeight();
//
//            if (inWidth > inHeight) {
//                outWidth = maxSize * inWidth / inHeight;
//                outHeight = maxSize;
//                cropW = (outWidth - outHeight) / 2;
//            } else {
//                outHeight = maxSize * inHeight / inWidth;
//                outWidth = maxSize;
//                cropH = (outHeight - outWidth) / 2;
//            }
//            bitmap = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, false);
//            bitmap = Bitmap.createBitmap(bitmap, cropW, cropH, maxSize, maxSize);

            bitmap = ThumbnailUtils.extractThumbnail(bitmap, maxSize, maxSize);

            //make
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            byte[] byteArray = output.toByteArray();
            String image = Base64.encodeToString(byteArray, Base64.NO_WRAP);
            updateGame("SMLTOWN.Social.android.setProfileImage('" + image + "')");
        }
    }

    // APPLICATION WORK ///////////////////////////////////////////////////////
    //
    public void closeApplication() {
        hideApplication();
        mainActivity.finish();
        System.exit(0);
    }

    public void hideApplication() { //by notification SWIPE release
        gameInBackground = true;
        mainActivity.moveTaskToBack(true);
        updateGame("SMLTOWN.Time.clearCountdowns()"); //stop any countdown
        updateGame("SMLTOWN.Server.ping = 60000"); //every 1min (only 4 ajax)
    }

    public static void updateGame(String js) {
        updateGame(js, "");
    }

    public static void updateGame(String js, String from) {
        String error = "not window.SMLTOWN yet! on " + actualUrl;
        if (!"".equals(from)) {
            error += " from " + from;
        }
        updatePage("if(window.SMLTOWN){" + js + "}else{console.log('" + error + "')}");
    }

    public static void updatePage(final String code) {
        lastInjection = code;
        webView.post(new Runnable() {
            public void run() {
                try {
                    if (code.length() > 150) {
                        Log.i(logName, "\n start INJECTION: \n" + code.substring(0, 80));
                        //Log.i(logName, "\n end INJECTION: \n" + code.substring(code.length() - 40));
                    } else {
                        Log.i(logName, "\n INJECTION: " + code + "\n");
                    }
                    webView.loadUrl("javascript:;" + code + ";");

                } catch (Exception e) {
                    String error = e.toString() + " on: " + code;
                    Log.i(logName, "JS ERROR: " + error);
                    //updateGame("smltown_error('" + error + "')");
                }
            }
        });
    }

    public static void updateCSS(String css) {
        //needs remove at least the last break line
        final String code = css.replace("\n", "").replace("\r", "");
        webView.post(new Runnable() {
            public void run() {
                // loadData brakes                

                String js = "var cssnode = document.createElement('style');"
                        + "cssnode.type = 'text/css';"
                        + "cssnode.innerHTML = '" + code + "';"
                        + "document.body.appendChild(cssnode);";
                //Log.i(logName, "injecting css: " + js);
                //webView.loadUrl("javascript:;" + js + ";");
            }
        });
    }

//    public void runJsFunction(String function) {
//        updateGame("if(window[" + function + "])" + function + "()");
//    }
    //
    //NOTIFICATIONS/////////////////////////////////////////////////////////////
    //
//    private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Connectivity con = new Connectivity();
//            if (con.isConnected(context)) {
//                updateGame("smltown_error('connected')");
//                resumeGame();
//            } else if (!con.isConnected(context)) {
//                updateGame("smltown_error('disconected')");
//                if (!deviceInfo.isAppForground()) {
//                    pauseGame(-1);
//                }
//            }
//        }
//    };
    //
    private final BroadcastReceiver enterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent dialogIntent = new Intent(getBaseContext(), gardiazabal.smltown.MainActivity.class
            );
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            getApplication()
                    .startActivity(dialogIntent);

            String gameId = intent.getAction().split(":")[0];
            int id = 0;

            try {
                id = Integer.parseInt(gameId);
            } catch (Exception e) {
                //
            }

            if (id > 0) {
                updateGame("SMLTOWN.Games.access(" + gameId + ")");
                //clear ALL notifications to prevent ghost games
                gameNotification.cleanNotifications();
            } else {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("gameRedirection", "").commit();
                updateGame("SMLTOWN.Load.showPage('gameList')");
            }
        }
    };

    private final BroadcastReceiver deleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            unregisterReceiver(this);
            hideApplication();
        }
    };

    public void addNotification(final String text, final String head, final String id, final String image, final boolean isChat) {
        //ServiceConnection have to be in MainActivity
        ServiceConnection mConnection = new ServiceConnection() {
            public
                    void onServiceConnected(ComponentName className, IBinder binder) {
                ((KillNotificationsService.KillBinder) binder).service.startService(new Intent(MainActivity.this, KillNotificationsService.class
                ));

                gameNotification.create(text, head, id, image, isChat);
            }

            public void onServiceDisconnected(ComponentName className) {
            }

        };
        bindService(new Intent(MainActivity.this, KillNotificationsService.class
        ), mConnection, Context.BIND_AUTO_CREATE);
    }

    // LAYOUT (FROM WIEWS) ////////////////////////////////////////////////////
    //
    public void chatFocusOut() {
        setBooleanTimeout("chat", 500);
        updateGame("chatFocusOut()");
    }

    public void setBooleanTimeout(String value, int time) {
        BackPressed.put(value, true);
        final String back = value;
        mHandler.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        BackPressed.put(back, false);
                    }
                },
                time); //prevent fast back of game when hide keyboard
    }

    private Runnable pingUpdate;

    public void updateGameTimeout(String code, int time) { //for ping
        mHandler.removeCallbacks(pingUpdate);
        final String js = code;
        pingUpdate = new Runnable() {
            @Override
            public void run() {
                updateGame(js);
            }
        };
        mHandler.postDelayed(pingUpdate, time);
    }

    //UTILS////////////////////////////////////////////////////////////////////
    //
    public Integer parseInt(String parse) {
        try {
            return Integer.parseInt(parse);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private boolean isNumeric(String str) {
        try {
            int d = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
