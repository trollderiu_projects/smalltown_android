package gardiazabal.smltown;

import static gardiazabal.smltown.MainActivity.*;
//import gardiazabal.smltown.MainActivity.stopRefreshLayout;
import android.webkit.JavascriptInterface;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.Header;
import java.io.IOException;
import android.app.*;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;
import android.content.Intent;
import android.provider.MediaStore;
import android.net.Uri;
import java.io.File;
import android.content.ActivityNotFoundException;
import android.widget.Toast;
import gardiazabal.smltown.utils.*;

// JAVASCRIPT functions /////////////////////////////////////////////////
//
public class WebAppInterface {

    MainActivity act;
    private String logName = this.getClass().getName();
    SharedPreferences.Editor editor;

    WebAppInterface(MainActivity application) {
        act = application;
        editor = act.prefs.edit();
    }

    public String handleGingerbreadStupidity = "javascript:;"
            + "function handler() {"
            //loadUrl
            + "this.loadUrl = function(url) {"
            + "    window.location = 'http://Device:loadUrl:' + url;"
            + "};"
            //getSmallTownHeader
            + "this.getSmallTownHeader = function(server) {"
            + "    return window.location = 'http://Device:getSmallTownHeader:' + server;"
            + "};"
            //getNotifications
            + "this.getNotifications = function(preference) {"
            + "    return window.location = 'http://Device:getNotifications:' + preference;"
            + "};"
            //gameRun
            + "this.gameRun = function(id) {"
            + "    window.location = 'http://Device:gameRun:' + id;"
            + "};"
            //exitGame
            + "this.exitGame = function() {"
            + "    window.location = 'http://Device:exitGame:';"
            + "};"
            //closeGame
            + "this.closeGame = function() {"
            + "    window.location = 'http://Device:closeGame:';"
            + "};"
            //setNotification
            + "this.setNotification = function(message, title, id) {"
            + "    var sep = '[Device]';"
            + "    window.location = 'http://Device:setNotification:' + encodeURIComponent(message + sep + title + sep + id);"
            + "};"
            //setChatNotification
            + "this.setChatNotification = function(message, title, id, image) {"
            + "    var sep = '[Device]';"
            + "    window.location = 'http://Device:setChatNotification:' + encodeURIComponent(message + sep + title + sep + id + sep + image);"
            + "};"
            //clearNotification
            + "this.clearNotification = function(id) {"
            + "    window.location = 'http://Device:clearNotification:' + id;"
            + "};"
            //vibrate
            + "this.vibrate = function() {"
            + "    window.location = 'http://Device:vibrate';"
            + "};"
            //setVibrationStrength
            + "this.setVibrationStrength = function(length) {"
            + "     window.location = 'http://Device:setVibrationStrength:' + length;"
            + "};"
            //savePreference
            + "this.savePreference = function(preference, value) {"
            + "    var sep = '[Device]';"
            + "    window.location = 'http://Device:savePreference:' + encodeURIComponent(preference + sep + value);"
            + "};"
            //getPreference
            + "this.getPreference = function(preference) {"
            + "    return window.location = 'http://Device:getPreference:' + preference;"
            + "};"
            //getInjection
            + "this.getInjection = function() {"
            + "    window.location = 'http://Device:getInjection';"
            + "};"
            //installServer
            + "this.installServer = function() {"
            + "    window.location = 'http://Device:installServer';"
            + "};"
            //clearCache
            + "this.clearCache = function() {"
            + "    window.location = 'http://Device:clearCache';"
            + "};"
            //getAndroidPicture
            + "this.getAndroidPicture = function(phoneId) {"
            + "    return window.location = 'http://Device:getAndroidPicture:' + phoneId;"
            + "};"
            //setPhone
            + "this.setPhone = function(phone) {"
            + "    return window.location = 'http://Device:setPhone:' + phone;"
            + "};"
            //findFriends
            + "this.findFriends = function() {"
            + "    window.location = 'http://Device:findFriends';"
            + "};"
            //saveFriends
            + "this.saveFriends = function(json) {"
            + "    window.location = 'http://Device:saveFriends:' + json;"
            + "};"
            //share
            + "this.share = function(share) {"
            + "    window.location = 'http://Device:share:' + share;"
            + "};"
            //stopRefresh
            + "this.stopRefresh = function() {"
            + "    window.location = 'http://Device:stopRefresh';"
            + "};"
            //pickUserImage
            + "this.pickUserImage = function() {"
            + "    window.location = 'http://Device:pickUserImage';"
            + "};"
            //referrerUrl
            + "this.referrerUrl = function() {"
            + "    window.location = 'http://Device:referrerUrl';"
            + "};"
            //copyToClipboard
            + "this.copyToClipboard = function(url) {"
            + "    window.location = 'http://Device:copyToClipboard:' + url;"
            + "};"
            //swipeGame
            + "this.swipeGame = function(value) {"
            + "    window.location = 'http://Device:swipeGame:' + value;"
            + "};"
            + "}"
            + "var Device = new handler();";

    @JavascriptInterface
    public void loadUrl(String url) {
        webView.clearView();
        webView.loadUrl(url);
    }

    @JavascriptInterface
    public String getSmallTownHeader(String server) { //server check on local
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(server.toLowerCase()); //server
            HttpResponse response = client.execute(request);
            Header header = response.getFirstHeader("smalltown");
            if (header != null) {
                return header.getValue(); //get header by 'key'
            }
        } catch (IOException e) {
            return e.toString();
        } catch (Exception e) {
            return e.toString();
        }
        return "";
    }

    @JavascriptInterface
    public String getNotifications() {
        return act.gameNotification.getJSONObject();
    }

    @JavascriptInterface
    public void gameRun(String id) {
        savePreference("gameRedirection", id);
        act.addNotification(null, null, id, "", false);
        int gameId = 0;
        try {
            gameId = Integer.parseInt(id);
        } catch (Exception e) {
            //
        }
        if (gameId > 0) {
            act.gameNotification.cleanNotifications(gameId);
        }
    }

    @JavascriptInterface
    public void exitGame() {
        savePreference("gameRedirection", "");
        act.gameNotification.remove();
    }

    @JavascriptInterface
    public void closeGame() {
        act.closeApplication();
    }

    @JavascriptInterface
    public void setNotification(String message, String title, String id) {
        act.addNotification(message, title, id, "", false);
    }

    @JavascriptInterface
    public void setChatNotification(String message, String title, String id, String image) {
        act.addNotification(message, title, id, image, true);
    }

    @JavascriptInterface
    public void clearNotification(String id) {
        try {
            if (!"".equals(id)) {
                int gameId = Integer.parseInt(id);
                act.gameNotification.cleanNotifications(gameId);
            } else {
                act.gameNotification.cleanNotifications();
            }

        } catch (Exception e) {
            Log.i(logName, id + " game id");
        }
        act.addNotification(null, null, id, "", false);
    }

    @JavascriptInterface
    public void vibrate() {
        if (act.gameInBackground) {
            //clearTimeout vibration
            act.updateGame("clearTimeout(SMLTOWN.temp.wakeUpInterval)");
            return;
        }
        act.vibration();
    }

    @JavascriptInterface
    public void setVibrationStrength(String length) {
        Integer num = act.parseInt(length);
        if (num != null) {
            act.pat[1] = num;
            act.pat[3] = num;
            act.vibration();
            savePreference("vibrationStrength", length);
        }
    }

    @JavascriptInterface
    public void savePreference(String preference, String value) {
        //Log.i(logName, "SAVE " + preference + ": " + value + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");        
        editor.putString(preference, value).commit();
    }

    @JavascriptInterface
    public String getPreference(String preference) {
        String value = act.prefs.getString(preference, "");
        //Log.i(logName, "GET " + preference + ": " +  value + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        return value;
    }

    @JavascriptInterface
    public void getInjection() {
//            updateCSS(deviceInfo.getRAW("css"));

        act.updateGame(act.deviceInfo.getAsset("libs/device.js"), "device.js");
        act.updateGame(act.deviceInfo.getRAW("intercept"), "intercept.js");
        act.updateGame(act.deviceInfo.getRAW("social"), "social.js");
        act.updateGame(act.deviceInfo.getRAW("index"), "index.js");

        //Log.i(logName, "act.updateCSS(act.deviceInfo.getRAW('css'));");
        act.updateCSS(act.deviceInfo.getRAW("css"));
    }

    @JavascriptInterface
    public void installServer() {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            //Log.e(logName, "!Environment.MEDIA_MOUNTED.equals(state)!!!!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        act.localServer.destFolder = Environment.getExternalStorageDirectory() + "/www/";
        // declare the dialog as a member field of your activity
        ProgressDialog mProgressDialog;

        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(act);

        act.localServer.copyServer(mProgressDialog);
    }

    @JavascriptInterface
    public void clearCache() {
        act.sdCardCache.clearApplicationCache();
    }

    @JavascriptInterface
    public String getAndroidPicture(String phoneId) {
        return act.deviceInfo.getAndroidPicture(phoneId);
    }

    @JavascriptInterface
    public String setPhone(String phone) {
        String hash = act.deviceInfo.hashPhone(phone);
        savePreference("phoneId", hash);
        return hash;
    }

//    //FRIENDS
//    @JavascriptInterface
//    public String getFriendsPhonesId() {
//        return act.deviceInfo.getFriendsPhonesId();
//    }
    @JavascriptInterface
    public void saveFriends(String json) {
        Log.i(logName, "FRIENDS = " + json);
        savePreference("friends", json);
    }

    @JavascriptInterface
    public void share(String url) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Come to play a Werewolf game!\r\n"
                + "on " + url + "\r\n";
//        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Come to play a Werewolf game!\r\n");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        act.startActivity(Intent.createChooser(sharingIntent, "Share a link to this game:"));
    }

    @JavascriptInterface
    public void stopRefresh() {
        act.stopRefreshLayout();
        /*getActivity().runOnUiThread(new Runnable() {
         @Override
         public void run() {
         swipeLayout.setRefreshing(false);
         }
         });*/
    }

    @JavascriptInterface
    public void pickUserImage() {
        Log.i(logName, "pickUserImage");

        try {
            //image
            Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
            pickIntent.setType("image/*");

            pickImage(pickIntent);

        } catch (ActivityNotFoundException e) {
            //OLD VERSIONS
            try {
                Intent pickIntent = new Intent(Intent.ACTION_PICK, null);
                pickIntent.setType("image/*");

                pickImage(pickIntent);

            } catch (ActivityNotFoundException e2) {
                Toast.makeText(act, "are google play services installed?", Toast.LENGTH_LONG).show();
                return;
            }
        }
    }

    @JavascriptInterface
    public String referrerUrl() {
        return act.actualUrl;
    }

    @JavascriptInterface
    public void copyToClipboard(String url) {
        MyClipboardManager clipboard = new MyClipboardManager();
        clipboard.copyToClipboard(act.ctx, url);

//        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
//        ClipData clip = ClipData.newPlainText(label, text);
//        clipboard.setPrimaryClip(clip);
    }

    @JavascriptInterface
    public void swipeGame(String dif) {
        Log.i(logName, "DIF = " + dif);
        if ("".equals(dif)) {//on stop swipe
            act.swipeLayout.setEnabled(true);
        } else {//on start swipe
            act.swipeLayout.setEnabled(false);
        }
    }

    public void pickImage(Intent pickIntent) {
        //photo
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        act.imageUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "fname_"
                + String.valueOf(System.currentTimeMillis()) + ".jpg"));
        takePhotoIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, act.imageUri);

        //choser
        Intent chooserIntent = Intent.createChooser(pickIntent, "Take or select a photo");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePhotoIntent});

        act.startActivityForResult(chooserIntent, 987);
    }

}
