package gardiazabal.smltown;

import android.app.ActivityManager;
import android.provider.ContactsContract;
import android.provider.Settings.Secure;
import android.accounts.AccountManager;
import android.accounts.Account;

import java.net.URLEncoder;
import java.util.List;
import android.content.ComponentName;
import android.database.Cursor;
import java.io.*;

import android.content.ContentResolver;
import android.content.Context;
import gardiazabal.smltown.utils.*;

import java.security.MessageDigest;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.net.Uri;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.ContentUris;
import android.util.Base64;
import java.security.*;
import android.util.Log;
import org.json.JSONArray;
import static gardiazabal.smltown.MainActivity.*;

public class DeviceInfo {

    Context ctx;
    private String logName = this.getClass().getName();

    DeviceInfo(Context context) {
        ctx = context;
    }

    //if app is running in showing 
    public boolean isAppForground() {
        ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(ctx.getPackageName())) {
                return false; //is back
            }
        }
        return true; //shows on top
    }

    // 1st time only
    public String[] getUserProfile(ContentResolver contentResolver) {
        String userEmail = null;
        String userName = null;
        String userId = Secure.getString(contentResolver, Secure.ANDROID_ID);

        //phoneHash
//        TelephonyManager tMgr = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
//        String mPhoneNumber = tMgr.getLine1Number();
//
        //GET PHONE NUMBER  CASE
//        String phoneId = null;
//        if (null != mPhoneNumber && !"".equals(mPhoneNumber)) {
//            phoneId = hashPhone(mPhoneNumber);
//        }
        //profile
//        if (android.os.Build.VERSION.SDK_INT > 13) { //new
//            Cursor c = contentResolver.query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);
//            if (c.moveToFirst()) {
//                userName = c.getString(c.getColumnIndex("DISPLAY_NAME"));
//                c.close();
//                return new String[]{userId, userName, phoneId};
//            }
//            c.close();
//        }
        //if not news version return
        AccountManager manager = (AccountManager) ctx.getSystemService(ctx.ACCOUNT_SERVICE);
        Account[] list = manager.getAccounts();
        for (Account account : list) {
            //if (emailPattern.matcher(account.name).matches()) {
            userEmail = account.name;
            userName = userEmail.split("@")[0];
            break;
            //}
            //if (account.type.equalsIgnoreCase("com.google")) {
            //    accountName = account.name;
            //    break;
            //}
        }

        return new String[]{userId, userName, userEmail};
    }

    public String getRAW(String name) { /* IMPORTANT: use this kind of comments! */
        // The InputStream opens the resourceId and sends it to the buffer

        InputStream is = ctx.getResources().openRawResource(
                ctx.getResources().getIdentifier("raw/" + name, "raw", ctx.getPackageName())
        );

        return readInputFile(is);
    }

    public String getAsset(String name) {
        InputStream is = null;
        try {
            is = ctx.getAssets().open(name);
        } catch (Exception e) {
            Log.i(logName, "ERROR ON GET ASSET: " + name);
            return "";
        }
        return readInputFile(is);
    }

    private String readInputFile(InputStream is) {
        StringBuilder raw = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String readLine = null;

        try {
            // While the BufferedReader readLine is not null 
            while ((readLine = br.readLine()) != null) {
//                raw += readLine + "\n"; //not work
                raw.append(readLine);
                raw.append('\n');
            }
            // Close the InputStream and BufferedReader
            is.close();
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        //return URLEncoder.encode(raw.toString()).replaceAll("\\+", " ");
        //an URLEncoder.encode fuction not disables all js
        return raw.toString();
    }

    //hash Phone
    private int length;
    private String n1;
    private String n2;
    private long number;
    private long number1;
    private long number2;

    public String hashPhone(String phone) {
        length = phone.length();
        n1 = phone.substring(0, length - 1);
        n2 = phone.substring(1, length);
        try {
            number = Integer.parseInt(phone);
            number1 = Integer.parseInt(n1);
            number2 = Integer.parseInt(n2);
        } catch (Exception e) {
            return null;
        }
        return "" + (number1 * number2 + number);
    }

    public String getAndroidPicture(String phoneId) {
        SharedPreferences prefs = ctx.getSharedPreferences("smalltown", Context.MODE_PRIVATE);
        String picture = null;

        //check stored
        String phone = prefs.getString(phoneId, "");
        if (!"".equals(phone)) {
            return getPictureFromPhone(phone);
        }

        Cursor contacts = getContactsPhone();
        while (contacts.moveToNext()) {
            String phoneNumber = contacts.getString(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            if (phoneId.equals(hashPhone(phoneNumber))) {
                //store friend phone                
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(phoneId, phoneNumber).commit();

                contacts.close();
                picture = getPictureFromPhone(phoneNumber);
                break;
            }
        }

        contacts.close();
        return picture;
    }

    private Cursor getContactsPhone() {
        return ctx.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
    }

    public String getPictureFromPhone(String phoneNumber) {
        ContentResolver contentResolver = ctx.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};
        Cursor cursor = contentResolver.query(uri, projection, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                break;
            }
            cursor.close();
            if (null == contactId) {
                return null;
            }
        }

        Bitmap bitmap;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(ctx.getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));
            if (null == inputStream) {
                Log.i(logName, "EMPTY STREAM IMAGE !!!!!!!!!!!!!!");
                return null;
            }
            bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        // Convert bitmap to Base64 encoded image for web
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Log.i(logName, "output = " + output);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
        byte[] byteArray = output.toByteArray();
        return Base64.encodeToString(byteArray, Base64.NO_WRAP);
    }

//    public String getFriendsPhonesId() {
//        JSONArray phones = new JSONArray();
//        Cursor contacts = getContactsPhone();
//        String phoneNumber;
//        String hash;
//        while (contacts.moveToNext()) {
//            phoneNumber = contacts.getString(contacts.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//            if (phoneNumber != null && !phoneNumber.isEmpty()) {
//                hash = hashPhone(phoneNumber);
//                if (null != hash) {
//                    phones.put(hash);
//                }
//            }
//        }
//        
//        return phones.toString();
//    }
    //    private void ckeckIfPremium() {
//        PackageManager manager = getPackageManager();
//        if (manager.checkSignatures(ctx.getPackageName(), "premium.gardiazabal.smltown") == PackageManager.SIGNATURE_MATCH) {
//            //updateGame("$('#log').text('OH YEAH!')");
//        }
//    }
}
