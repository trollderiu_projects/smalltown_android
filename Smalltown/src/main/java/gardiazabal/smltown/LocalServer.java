package gardiazabal.smltown;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.sql.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.Map;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import gardiazabal.smltown.utils.*;
import static gardiazabal.smltown.MainActivity.*;

public class LocalServer {
    
    private String logName = this.getClass().getName();
    Context ctx;
    String destFolder;
    File destFolderFile;
    Boolean inGame = false;

    LocalServer(Context context) {
        ctx = context;
        destFolderFile = new File(Environment.getExternalStorageDirectory() + "/www/smalltown");
    }

    public void installServer(ProgressDialog d) {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            Log.e(logName, "!Environment.MEDIA_MOUNTED.equals(state)!!!!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        destFolder = Environment.getExternalStorageDirectory() + "/www/";
        copyServer(d);
    }

    public void removeServer() {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            Log.e(logName, "!Environment.MEDIA_MOUNTED.equals(state)!!!!!!!!!!!!!!!!!!!!!!!");
            return;
        }

        destFolder = Environment.getExternalStorageDirectory() + "/www/";
        removeAll();
    }

    private void removeAll() {
        DeleteRecursive(new File(destFolder + "smalltown.zip"));
        DeleteRecursive(new File(destFolder + "smalltown"));
    }

    protected void copyServer(ProgressDialog mProgressDialog) {
        final DownloadFilesTask downloadTask = new DownloadFilesTask();

        mProgressDialog.setMessage("A message");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

// execute this when the downloader must be fired
		/*final DownloadTask downloadTask = new DownloadTask(this);
         downloadTask.execute("the url to the file you want to download");*/
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });

        // First: checking if there is already a target folder
        removeAll();
        String link = "https://github.com/smalltowngame/webapp/archive/master.zip";

        // Moving all the files on external SD
        try {
            URL url = new URL(link);
            downloadTask.execute(url);
        } catch (IOException e) {
            Log.e(logName, "Failed to copy asset file: " + link + "", e);
        }
    }

    private class DownloadFilesTask extends AsyncTask<URL, Integer, Boolean> {

//        protected void onPreExecute() {
//
//        }
        @Override
        protected Boolean doInBackground(URL... urls) {
            String link;
            URL url = urls[0];

            try {
                HttpURLConnection http = (HttpURLConnection) url.openConnection();

                Map< String, List< String>> header = http.getHeaderFields();
                while (isRedirected(header)) {
                    link = header.get("Location").get(0);
                    url = new URL(link);
                    http = (HttpURLConnection) url.openConnection();
                    header = http.getHeaderFields();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = http.getContentLength();

                InputStream in = http.getInputStream();
                OutputStream out = new FileOutputStream(destFolder + "smalltown.zip");

                byte[] buffer = new byte[1024];
                int read;
                long total = 0;
                while ((read = in.read(buffer)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        in.close();
                        return null;
                    }
                    total += read;
                    // publishing the progress....
                    if (fileLength > 0) { // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    }
                    out.write(buffer, 0, read);
                }

                in.close();
                out.flush();
                out.close();
            } catch (Exception e) {
                return false;
            }
            return true;
        }

//        protected void onProgressUpdate(Integer... progress) {
////            setProgressPercent(progress[0]);
//        }
        protected void onPostExecute(Boolean done) {
//            if (!done) {
//                return;
//            }
//            showDialog("Downloaded ");
            ZipUtils zipUtils = new ZipUtils();
            zipUtils.extract(new File(destFolder + "smalltown.zip"), new File(destFolder), true);

            File file = new File(destFolder + "smalltown-master");
            File file2 = new File(destFolder + "smalltown");
            boolean success = file.renameTo(file2);
        }
    }

    private boolean isRedirected(Map<String, List<String>> header) {
        for (String hv : header.get(null)) {
            if (hv.contains(" 301 ") || hv.contains(" 302 ")) {
                return true;
            }
        }
        return false;
    }

    private void DeleteRecursive(File fileOrDirectory) {
        if (!fileOrDirectory.exists()) {
            return;
        }
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                DeleteRecursive(child);
            }
        }
        fileOrDirectory.delete();
    }

}
