package gardiazabal.smltown;

import android.app.Service;
import android.content.*;
import android.os.*;

public class BootService extends Service {

    @Override
    public IBinder onBind(Intent p1) {
        // TODO: Implement this method
        return null;
    }

    @Override
    public void onCreate() {
        GameNotification gameNotifiaction = new GameNotification(this, enterReceiver, deleteReceiver, true);
        gameNotifiaction.create();
    }

    private final BroadcastReceiver enterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            
        }
    };
    private final BroadcastReceiver deleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            
        }
    };
}
