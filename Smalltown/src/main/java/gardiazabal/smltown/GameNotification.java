package gardiazabal.smltown;

import static gardiazabal.smltown.MainActivity.*;
import gardiazabal.smltown.utils.*;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.IntentFilter;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.app.Notification;
import android.util.*;
import java.util.List;
import java.util.ArrayList;
import android.widget.*;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.res.Resources;
import org.json.JSONObject;
import org.json.*;
import android.text.Html;

public class GameNotification {

    Context ctx;
    private String logName = this.getClass().getName();
    BroadcastReceiver enterReceiver;
    BroadcastReceiver deleteReceiver;
    boolean old;

    NotificationCompat.Builder builder;
    NotificationManager notificationManager;
    DeviceInfo deviceInfo;

    int mainId = 0;
    int chatMessages = 0;
    public List<Integer> games = new ArrayList<Integer>();
    public List<Note> notifications = new ArrayList<Note>();

    public GameNotification(Context context, BroadcastReceiver enter, BroadcastReceiver delete, boolean oldVersion) {
        ctx = context;
        enterReceiver = enter;
        deleteReceiver = delete;
        old = oldVersion;

        deviceInfo = new DeviceInfo(ctx);
        notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void create() {
        create("desde servicio", "prueba", "0", null, false);
    }

    public void create(String message, String title, String id, String image, boolean isChat) {
        int gameId = -1;

        if (null == id || id.isEmpty()) {
            gameId = mainId;
        } else {
            try {
                gameId = Integer.parseInt(id);
            } catch (Exception e) {
                //
            }
        }
        //int gameId
        create(message, title, gameId, image, isChat);
    }

    public void create(String message, String title, int gameId, String image, boolean isChat) {
        builder = new NotificationCompat.Builder(ctx); //redo all builder

        if (null != message) {
            if (!games.contains(gameId)) {
                games.add(gameId);
            }
            Note note = new Note(gameId, title, message, isChat);

            //remove older not chat notifications
            if (!isChat) {
                for (int i = 0; i < notifications.size(); i++) {
                    Note notification = notifications.get(i);
                    if (gameId == notification.gameId && !notification.isChat) {
                        notifications.remove(i);
                    }
                }
            }

            notifications.add(0, note);
        }
        if (games.size() > 0) {
            if (games.contains(mainId)) {
                gameId = mainId;
            } else {
                gameId = 0; //to gameList
            }
        }

        String filter = gameId + ":" + (isChat ? "true" : "false");
        Intent enterIntent = new Intent(filter);
        PendingIntent enterPendingIntent = PendingIntent.getBroadcast(ctx, 0, enterIntent, 0);
        ctx.registerReceiver(enterReceiver, new IntentFilter(filter));

        if (null != message) { //if some game id            
            builder.setSmallIcon(R.drawable.ic_notification_active);

            if (games.size() > 1 || notifications.size() > 1) {
                title += ": " + message;
                message = notifications.size() + " notifications, from " + games.size() + " games";
                if (games.size() < 2) {
                    message = (notifications.size() - 1) + " notifications more.";
                }

                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

                boolean isTitle = false;
                int lines = 0;

                //main notifications
                for (int i = 0; i < notifications.size(); i++) {
                    Note n = notifications.get(i);
                    if (!n.isChat && n.gameId == mainId) {
                        String line = n.title + ": " + n.text;

                        if (!isTitle) {
                            inboxStyle.setBigContentTitle(Html.fromHtml(line));
                            isTitle = true;
                        } else if (8 > lines) {
                            inboxStyle.addLine(Html.fromHtml(line));
                        } else {
                            break;
                        }
                        lines++;
                    }
                }

                //other notifications
                for (int i = 0; i < notifications.size(); i++) {
                    Note n = notifications.get(i);
                    if (!n.isChat && n.gameId != mainId) {
                        String line = n.title + ": " + n.text;

                        if (!isTitle) {
                            inboxStyle.setBigContentTitle(Html.fromHtml(line));
                            isTitle = true;
                        } else if (8 > lines) {
                            inboxStyle.addLine(Html.fromHtml(line));
                        } else {
                            break;
                        }
                        lines++;
                    }
                }

                //chat
                for (int i = 0; i < notifications.size(); i++) {
                    Note n = notifications.get(i);
                    if (n.isChat) {
                        String chatTitle = n.title;

                        //find 1st name
                        int index = chatTitle.indexOf(' ', 4);
                        if (index > -1) {
                            chatTitle = chatTitle.substring(0, index) + "..";
                            //limit name length
                        } else if (chatTitle.length() > 15) {
                            chatTitle = chatTitle.substring(0, 15) + "..";
                        }
                        String line = "<font color='#AAAAAA'>" + chatTitle + "</font> ";
                        if (n.gameId != mainId) {
                            line += "<font color='#DDDDDD'>" + n.gameId + "</font> ";
                        }
                        line += n.text;

                        if (!isTitle) {
                            inboxStyle.setBigContentTitle(Html.fromHtml(line));
                            isTitle = true;
                        } else if (8 > lines) {
                            inboxStyle.addLine(Html.fromHtml(line));
                        } else {
                            break;
                        }
                        lines++;
                    }
                }

                if (notifications.size() > 8) {
                    int rest = notifications.size() - 8;
                    inboxStyle.setSummaryText("+" + rest + " more");
                } else {
                    inboxStyle.setSummaryText(notifications.size() + " new messages");
                }

                builder.setStyle(inboxStyle);
                
            }else if(!isChat){
                //single game notification
                title = "game " + title;
            }

            Intent deleteIntent = new Intent("deletedChat");
            PendingIntent deletePendingIntent = PendingIntent.getBroadcast(ctx, 0, deleteIntent, 0);
            ctx.registerReceiver(swipe, new IntentFilter("deletedChat"));
            builder.setDeleteIntent(deletePendingIntent);
            if (!"".equals(image)) {
                image = image.replace("\"", "");
                Bitmap bitmap = getBitmap(image);
                builder.setLargeIcon(bitmap);
            }

            //type
            if (isChat) { //chat
                chatMessages++;
                if (deviceInfo.isAppForground()) { //if app shown
                    return;
                }
                //compat cant set default vibration here
                builder.setPriority(NotificationCompat.PRIORITY_HIGH).setVibrate(new long[0]);

                title += ":";
                //
            } else { //notification
                if (null == title) {
                    title = "in-game notification";
                }
            }
            //
        } else { //no message -> main

            if (gameId < 1) {
                //boot call and errors.. //RUN MAIN ACTIVITY?
                enterIntent = new Intent(ctx, gardiazabal.smltown.MainActivity.class);
                enterPendingIntent = PendingIntent.getActivity(ctx, 0, enterIntent, 0);
            }

            //MAIN NOTIFICATION
            message = "Touch to resume";
            //if (!old) { //if !android 2.3
            if (android.os.Build.VERSION.SDK_INT >= 11 && !old) {
                message = "Swipe to run on background";
            }
            if (gameId > 0) {
                mainId = gameId;
            }

            Intent deleteIntent = new Intent("deleted");
            PendingIntent deletePendingIntent = PendingIntent.getBroadcast(ctx, 0, deleteIntent, 0);
            ctx.registerReceiver(deleteReceiver, new IntentFilter("deleted"));
            builder.setDeleteIntent(deletePendingIntent)
                    .setPriority(Notification.PRIORITY_LOW)
                    .setSmallIcon(R.drawable.ic_notification);
        }

        if (null == title) {
            title = "Full running mode";
        }

        Notification n = builder
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(enterPendingIntent)
                .build();

        if (isChat) { //chat
            n.defaults |= Notification.DEFAULT_VIBRATE;
        }

        notificationManager.notify(0, n);
    }

    public void cleanNotifications() {      
        Log.i(logName, "clear!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        notifications.clear();
        games.clear();
    }

    public void cleanNotifications(int id) {
        for (int i = 0; i < notifications.size(); i++) {
            if (id == notifications.get(i).gameId) {
                notifications.remove(i);
            }
        }
        for (int i = 0; i < games.size(); i++) {
            if (id == games.get(i)) {
                games.remove(i);
                return;
            }
        }
    }

    private Bitmap getBitmap(String myImageData) {
        byte[] imageAsBytes = Base64.decode(myImageData.getBytes(), 0);
        Bitmap bmURL = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
        return Bitmap.createScaledBitmap(bmURL, (int) convertDpToPixel(64), (int) convertDpToPixel(64), false);
    }

    public float convertDpToPixel(float dp) {
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    private final BroadcastReceiver swipe = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateGame("clearTimeout(SMLTOWN.inject.vibrationInterval)"); //injected
            if (mainId > 0) {
                create(null, null, mainId, null, false);
            }
        }
    };

    public void remove() { //@JavascriptInterface exitGame()
        mainId = 0;

        if (wakeLock.isHeld()) {
            wakeLock.release(); //let sleep
        }
        String ns = ctx.NOTIFICATION_SERVICE;
        if (null != ns) {
            NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
            nMgr.cancel(0);
        }
    }

    public String getJSONObject() {
        JSONArray json = new JSONArray();
        for (Note n : notifications) {
            JSONObject note = new JSONObject();
            try {
                note.put("id", n.gameId);
                note.put("title", n.title);
                note.put("text", n.text);
                note.put("isChat", n.isChat);
                json.put(note);
            } catch (Exception e) {

            }
        }
        return json.toString();
    }

    class Note {

        int gameId;
        String title;
        String text;
        boolean isChat;

        Note(int id, String t, String txt, boolean chat) {
            gameId = id;
            title = t;
            text = txt;
            isChat = chat;
        }
    }

}
