package gardiazabal.smltown.utils;

import java.io.File;
import android.os.Environment;
import android.content.Context;
import android.util.*;
import static gardiazabal.smltown.MainActivity.*;

//http://www.devahead.com/blog/2012/01/saving-the-android-webview-cache-on-the-sd-card/
public class SDCardCache {
    // NOTE: the content of this path will be deleted when the application is uninstalled (Android 2.2 and higher)
    
    private String logName = this.getClass().getName();
    private Context ctx;
    protected File extStorageAppBasePath;
    protected File extStorageAppCachePath;

    public SDCardCache(Context c) {
        ctx = c;
    }

    public void onCreate() {

        // Check if the external storage is writeable
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            // Retrieve the base path for the application in the external storage
            File externalStorageDir = Environment.getExternalStorageDirectory();

            if (externalStorageDir != null) {
                // {SD_PATH}/Android/data/com.devahead.androidwebviewcacheonsd
                extStorageAppBasePath = new File(externalStorageDir.getAbsolutePath()
                        + File.separator + "Android" + File.separator + "data"
                        + File.separator + ctx.getPackageName());
            }

            if (extStorageAppBasePath != null) {
                // {SD_PATH}/Android/data/com.devahead.androidwebviewcacheonsd/cache
                extStorageAppCachePath = new File(extStorageAppBasePath.getAbsolutePath()
                        + File.separator + "cache");

                boolean isCachePathAvailable = true;

                if (!extStorageAppCachePath.exists()) {
                    // Create the cache path on the external storage
                    isCachePathAvailable = extStorageAppCachePath.mkdirs();
                }

                if (!isCachePathAvailable) {
                    // Unable to create the cache path
                    extStorageAppCachePath = null;
                }
            }
        }
    }

    public File getCacheDir() {
        // NOTE: this method is used in Android 2.2 and higher
        if (extStorageAppCachePath != null) {
            // Use the external storage for the cache
            return extStorageAppCachePath;
        }
        // /data/data/com.devahead.androidwebviewcacheonsd/cache
        return null;
    }

    public int clearApplicationCache() {

//                webView.clearCache(true);
//                getApplicationContext().deleteDatabase("webview.db");
//                getApplicationContext().deleteDatabase("webviewCache.db");
//
//                CookieSyncManager.createInstance(this);
//                CookieManager cookieManager = CookieManager.getInstance();
//                cookieManager.removeAllCookie();
        //
        File dir = ctx.getCacheDir();
        return clearCacheFolder(dir);
    }

    public int clearCacheFolder(File dir) {
        int deletedFiles = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {

                    //first delete subdirectories recursively
                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child);
                    }

                    //then delete the files and subdirectories in this dir
                    //only empty directories can be deleted, so subdirs have been done first
                    if (child.delete()) {
                        deletedFiles++;
                    }
                }
            } catch (Exception e) {
                Log.e(logName, String.format("Failed to clean the cache, error %s", e.getMessage()));
            }
        }
        return deletedFiles;
    }

}
